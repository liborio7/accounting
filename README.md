# Revolut money transfer exercise

A RESTful API for money transfers between accounts.
The accounts and transfers between them will be written into an in-memory database.
On server startup, all data structures will be created if not already present.

The API were implemented in **Java 8** using **Gradle** for build automation and dependency management.

See [API documentation](/apidoc.md) for further details. 


### Libraries

- **Grizzly 2 Http Server Container**, an embedded server with **HK2** for dependency injection
- **Jersey JSON Jackson**, for JSON serialization and deserialization
- **H2 Database Engine**, an in-memory database
- **MyBatis SQL mapper framework**, a persistence framework with support for custom SQL
- **Logback classic module**, for logging
- **Apache Commons Lang**, helper utilities for the java.lang API
- **JUnit testing framework**, for unit tests
- **AssertJ core**, for fluent assertions
- **Mockito core**, a mocking framework for unit tests
- **Random Beans core implementation**, to generate random java beans for testing purpose
- **Jersey Test Framework**, for integration tests


### Features

- Account creation (EUR as default currency)
- Money transfer between 2 existing accounts
- Thread safety with pessimistic locking on money transfer execution
- Optimistic locking implementation
- Separation of persistence model, business logic, and HTTP resources layers
- RESTful HTTP response codes
- Unit tests
- Integration tests


### Requirements

- Java
- Gradle


### Get started

1. Clone GIT repository: ``git clone git@bitbucket.org:liborio7/accounting.git``
2. CD into the project directory to build the project: ``./gradlew build``
3. Run tests: ``./gradlew test``
4. Run server: ``./gradlew run``


### Future improvements

- Idempotency on accounts and, especially, transfers insertion through a request id/token
- API to retrieve transfers by sender/receiver ID
- Pagination on get all accounts/transfers API responses
- Additional error code field to specify the reason why a request failed
- Multiple currency and exchange rate on transfers
- Account states: enabled/disabled
- Transfer states: pending/accepted/canceled
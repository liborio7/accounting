# REST API documentation

* _POST v1/accounts_
* _GET v1/accounts_
* _GET v1/accounts/{id}_
* _POST v1/transfers_
* _GET v1/transfers_
* _GET v1/transfers/{id}_

## [POST] accounts

    POST v1/accounts

Create a new account.
The amount is expressed in the smallest EUR currency units (ex. 1.15€ = 115 units).

### Body
- **amount_unit** — The initial amount for the balance account, must be greater than 0.

### Response
- **id** — The account ID
- **version** — The account version for optimistic locking
- **amount_unit** — The account balance amount

### Errors
- **400** - Invalid amount or empty request.
- **500** - Unhandled internal server error

### Example
**Request**

    https://localhost:9999/v1/accounts

``` json
{
    "amount_unit": 400
}
```

**Return** 
``` json
{
    "id": "9bd618ca-ac7f-4dbc-b5c1-f2f2c9ded3f2",
    "version": 0,
    "amount_unit": 400
}
```

## [GET] accounts/{id}

    GET v1/accounts/{id}

Get a specific account by ID.

### Path param
- **id** — The account ID

### Response
- **id** — The account ID
- **version** — The account version for optimistic locking
- **amount_unit** — The account balance amount

### Errors
- **404** - Account not found
- **500** - Unhandled internal server error

### Example
**Request**

    https://localhost:9999/v1/accounts/9bd618ca-ac7f-4dbc-b5c1-f2f2c9ded3f2

**Return** 
``` json
{
    "id": "9bd618ca-ac7f-4dbc-b5c1-f2f2c9ded3f2",
    "version": 0,
    "amount_unit": 400
}
```

## [GET] accounts

    GET v1/accounts

List all accounts.

### Response
Array list of:

- **id** — The account ID
- **version** — The account version for optimistic locking
- **amount_unit** — The account balance amount

### Errors
- **500** - Unhandled internal server error

### Example
**Request**

    https://localhost:9999/v1/accounts

**Return** 
``` json
[
    {
        "id": "ea8e8a78-3558-49b6-afaa-fca953b522c1",
        "version": 1,
        "amount_unit": 0
    },
    {
        "id": "9bd618ca-ac7f-4dbc-b5c1-f2f2c9ded3f2",
        "version": 0,
        "amount_unit": 400
	}
]
```

## [POST] transfers

    POST v1/transfers

Create a new transfer.
The amount is expressed in the smallest EUR currency units (ex. 1.15€ = 115 units).

### Body
- **sender_account_id** - The account ID from which money will be withdrawn
- **receiver_account_id** - The account ID to which money will be deposited
- **amount_unit** — The amount to transfer

### Response
- **id** — The transfer ID
- **version** — The transfer version for optimistic locking
- **sender_account_id** - The account ID from which money has been withdrawn
- **receiver_account_id** - The account ID to which money has been deposited
- **amount_unit** — The transfer amount

### Errors
- **400** - Invalid amount, insufficient availability, invalid sender, invalid receiver or empty request.
- **500** - Unhandled internal server error

### Example
**Request**

    https://localhost:9999/v1/transfers

``` json
{
    "sender_account_id": "29a26481-f81c-42b1-84f8-fadd5347f31e",
    "receiver_account_id": "9a5f80ef-6253-4076-b4f0-106c0a864e3e",
    "amount_unit": 10
}
```

**Return** 
``` json
{
    "id": "5ce957a4-e3cd-469e-b303-e444fe8826b8",
    "version": 0,
    "amount_unit": 10,
    "sender_account_id": "29a26481-f81c-42b1-84f8-fadd5347f31e",
    "receiver_account_id": "9a5f80ef-6253-4076-b4f0-106c0a864e3e"
}
```

## [GET] transfers/{id}

    GET v1/transfers/{id}

Get a specific transfer by ID.

### Path param
- **id** — The transfer ID

### Response
- **id** — The transfer ID
- **version** — The transfer version for optimistic locking
- **sender_account_id** - The account ID from which money has been withdrawn
- **receiver_account_id** - The account ID to which money has been deposited
- **amount_unit** — The transfer amount

### Errors
- **404** - Transfer not found
- **500** - Unhandled internal server error

### Example
**Request**

    https://localhost:9999/v1/transfers/

**Return** 
``` json
{
    "id": "5ce957a4-e3cd-469e-b303-e444fe8826b8",
    "version": 0,
    "amount_unit": 10,
    "sender_account_id": "29a26481-f81c-42b1-84f8-fadd5347f31e",
    "receiver_account_id": "9a5f80ef-6253-4076-b4f0-106c0a864e3e"
}
```

## [GET] transfers

    GET v1/transfers

List all transfers.

### Response
Array list of:

- **id** — The transfer ID
- **version** — The transfer version for optimistic locking
- **sender_account_id** - The account ID from which money has been withdrawn
- **receiver_account_id** - The account ID to which money has been deposited
- **amount_unit** — The transfer amount

### Errors
- **500** - Unhandled internal server error

### Example
**Request**

    https://localhost:9999/v1/transfers

**Return** 
``` json
[
    {
        "id": "5ce957a4-e3cd-469e-b303-e444fe8826b8",
        "version": 0,
        "amount_unit": 10,
        "sender_account_id": "29a26481-f81c-42b1-84f8-fadd5347f31e",
        "receiver_account_id": "9a5f80ef-6253-4076-b4f0-106c0a864e3e"
    },
    {
        "id": "4afcaeea-d99b-409d-94c9-d816a88609e4",
        "version": 0,
        "amount_unit": 10,
        "sender_account_id": "29a26481-f81c-42b1-84f8-fadd5347f31e",
        "receiver_account_id": "9a5f80ef-6253-4076-b4f0-106c0a864e3e"
	}
]
```
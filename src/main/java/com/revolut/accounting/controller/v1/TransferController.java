package com.revolut.accounting.controller.v1;

import com.revolut.accounting.business.TransferService;
import com.revolut.accounting.business.bean.TransferBean;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Singleton
@Path("/v1/transfers")
public class TransferController {

    @Inject private TransferService service;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public TransferBean insert(TransferBean accountBean) {
        return service.insert(accountBean);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransferBean> loadAll() {
        return service.loadAll();
    }

    @GET
    @Path("{id: [0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}}")
    @Produces(MediaType.APPLICATION_JSON)
    public TransferBean loadById(@PathParam("id") UUID id) {
        return service.loadById(id);
    }
}

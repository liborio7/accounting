package com.revolut.accounting.controller.v1;

import com.revolut.accounting.business.AccountService;
import com.revolut.accounting.business.bean.AccountBean;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Singleton
@Path("/v1/accounts")
public class AccountController {

    @Inject private AccountService service;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public AccountBean insert(AccountBean accountBean) {
        return service.insert(accountBean);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<AccountBean> loadAll() {
        return service.loadAll();
    }

    @GET
    @Path("{id: [0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}}")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountBean loadById(@PathParam("id") UUID id) {
        return service.loadById(id);
    }
}

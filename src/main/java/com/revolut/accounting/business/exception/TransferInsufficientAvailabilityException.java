package com.revolut.accounting.business.exception;

public class TransferInsufficientAvailabilityException extends Exception {

    public TransferInsufficientAvailabilityException() {
        super("insufficient availability");
    }
}

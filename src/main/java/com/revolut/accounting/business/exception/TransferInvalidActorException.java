package com.revolut.accounting.business.exception;

public class TransferInvalidActorException extends Exception {

    public TransferInvalidActorException() {
        super("invalid actor");
    }
}

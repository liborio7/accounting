package com.revolut.accounting.business.converter;

import com.revolut.accounting.business.bean.TransferBean;
import com.revolut.accounting.business.bean.TransferBeanBuilder;
import com.revolut.accounting.persistence.model.Transfer;
import com.revolut.accounting.persistence.model.TransferBuilder;

import javax.inject.Singleton;

@Singleton
public class TransferConverter {

    public TransferBean modelToBean(Transfer model) {
        return new TransferBeanBuilder()
                .withId(model.getId())
                .withVersion(model.getVersion())
                .withAmountUnit(model.getAmountUnit())
                .withSenderAccountId(model.getSenderAccountId())
                .withReceiverAccountId(model.getReceiverAccountId())
                .build();
    }

    public Transfer beanToModel(TransferBean bean) {
        return new TransferBuilder()
                .withId(bean.getId())
                .withVersion(bean.getVersion())
                .withAmountUnit(bean.getAmountUnit())
                .withSenderAccountId(bean.getSenderAccountId())
                .withReceiverAccountId(bean.getReceiverAccountId())
                .build();
    }
}

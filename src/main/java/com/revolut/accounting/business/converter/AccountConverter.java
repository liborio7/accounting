package com.revolut.accounting.business.converter;

import com.revolut.accounting.business.bean.AccountBean;
import com.revolut.accounting.business.bean.AccountBeanBuilder;
import com.revolut.accounting.persistence.model.Account;
import com.revolut.accounting.persistence.model.AccountBuilder;

import javax.inject.Singleton;

@Singleton
public class AccountConverter {

    public AccountBean modelToBean(Account model) {
        return new AccountBeanBuilder()
                .withId(model.getId())
                .withVersion(model.getVersion())
                .withAmountUnit(model.getAmountUnit())
                .build();
    }

    public Account beanToModel(AccountBean bean) {
        return new AccountBuilder()
                .withId(bean.getId())
                .withVersion(bean.getVersion())
                .withAmountUnit(bean.getAmountUnit())
                .build();
    }

}

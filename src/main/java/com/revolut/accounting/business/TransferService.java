package com.revolut.accounting.business;

import com.revolut.accounting.business.bean.TransferBean;
import com.revolut.accounting.business.converter.TransferConverter;
import com.revolut.accounting.business.exception.TransferInsufficientAvailabilityException;
import com.revolut.accounting.business.exception.TransferInvalidActorException;
import com.revolut.accounting.persistence.dao.TransferDao;
import com.revolut.accounting.persistence.model.Transfer;
import com.revolut.accounting.persistence.model.TransferBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Singleton
public class TransferService {
    private final Logger logger = LoggerFactory.getLogger(TransferService.class);

    @Inject private TransferConverter converter;
    @Inject private TransferDao dao;

    //============ INSERT

    public TransferBean insert(TransferBean transferBean) {
        logger.info("insert: {}", transferBean);

        // validate input
        if (transferBean == null) {
            throw new BadRequestException("invalid request");
        }
        if (transferBean.getAmountUnit() == null || transferBean.getAmountUnit() <= 0) {
            throw new BadRequestException("invalid amount");
        }
        if (transferBean.getSenderAccountId() == null) {
            throw new BadRequestException("invalid sender");
        }
        if (transferBean.getReceiverAccountId() == null) {
            throw new BadRequestException("invalid receiver");
        }

        try {
            Transfer transferToInsert = converter.beanToModel(transferBean);
            Transfer transferInserted = dao.insert(transferToInsert);
            return converter.modelToBean(transferInserted);

        } catch (TransferInvalidActorException | TransferInsufficientAvailabilityException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    //============ LOAD ALL

    public List<TransferBean> loadAll() {
        logger.info("load all");

        return dao.loadAll()
                .stream()
                .map(converter::modelToBean)
                .collect(Collectors.toList());
    }

    //============ LOAD BY ID

    public TransferBean loadById(UUID id) {
        logger.info("load by id: {}", id);

        // load
        return Optional.ofNullable(dao.loadById(new TransferBuilder().withId(id).build()))
                .map(converter::modelToBean)
                .orElseThrow(NotFoundException::new);
    }
}

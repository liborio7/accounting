package com.revolut.accounting.business;

import com.revolut.accounting.business.bean.AccountBean;
import com.revolut.accounting.business.converter.AccountConverter;
import com.revolut.accounting.persistence.dao.AccountDao;
import com.revolut.accounting.persistence.model.Account;
import com.revolut.accounting.persistence.model.AccountBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Singleton
public class AccountService {
    private final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Inject private AccountConverter converter;
    @Inject private AccountDao dao;

    //============ INSERT

    public AccountBean insert(AccountBean accountBean) {
        logger.info("insert: {}", accountBean);

        // validate input
        if (accountBean == null) {
            throw new BadRequestException("invalid request");
        }
        if (accountBean.getAmountUnit() == null || accountBean.getAmountUnit() <= 0) {
            throw new BadRequestException("invalid amount");
        }

        Account accountToInsert = converter.beanToModel(accountBean);
        Account accountInserted = dao.insert(accountToInsert);
        return converter.modelToBean(accountInserted);
    }

    //============ LOAD ALL

    public List<AccountBean> loadAll() {
        logger.info("load all");

        return dao.loadAll()
                .stream()
                .map(converter::modelToBean)
                .collect(Collectors.toList());
    }

    //============ LOAD BY ID

    public AccountBean loadById(UUID id) {
        logger.info("load by id: {}", id);

        return Optional.ofNullable(dao.loadById(new AccountBuilder().withId(id).build()))
                .map(converter::modelToBean)
                .orElseThrow(NotFoundException::new);
    }
}

package com.revolut.accounting.business.bean;

import java.util.UUID;

public class AccountBean {
    private UUID id;
    private Integer version;
    private Long amountUnit;

    public AccountBean() {
    }

    public AccountBean(UUID id, Integer version, Long amountUnit) {
        this.id = id;
        this.version = version;
        this.amountUnit = amountUnit;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getAmountUnit() {
        return amountUnit;
    }

    public void setAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
    }
}

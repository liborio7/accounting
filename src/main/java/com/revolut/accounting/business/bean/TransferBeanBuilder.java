package com.revolut.accounting.business.bean;

import java.util.UUID;

public class TransferBeanBuilder {
    private UUID id;
    private Integer version;
    private Long amountUnit;
    private UUID senderAccountId;
    private UUID receiverAccountId;

    public TransferBeanBuilder withId(UUID id) {
        this.id = id;
        return this;
    }

    public TransferBeanBuilder withVersion(Integer version) {
        this.version = version;
        return this;
    }

    public TransferBeanBuilder withAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
        return this;
    }

    public TransferBeanBuilder withSenderAccountId(UUID senderAccountId) {
        this.senderAccountId = senderAccountId;
        return this;
    }

    public TransferBeanBuilder withReceiverAccountId(UUID receiverAccountId) {
        this.receiverAccountId = receiverAccountId;
        return this;
    }

    public TransferBean build() {
        return new TransferBean(id, version, amountUnit, senderAccountId, receiverAccountId);
    }
}
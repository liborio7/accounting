package com.revolut.accounting.business.bean;

import java.util.UUID;

public class AccountBeanBuilder {
    private UUID id;
    private Integer version;
    private Long amountUnit;

    public AccountBeanBuilder withId(UUID id) {
        this.id = id;
        return this;
    }

    public AccountBeanBuilder withVersion(Integer version) {
        this.version = version;
        return this;
    }

    public AccountBeanBuilder withAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
        return this;
    }

    public AccountBean build() {
        return new AccountBean(id, version, amountUnit);
    }
}
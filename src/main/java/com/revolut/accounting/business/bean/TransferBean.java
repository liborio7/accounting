package com.revolut.accounting.business.bean;

import java.util.UUID;

public class TransferBean {
    private UUID id;
    private Integer version;
    private Long amountUnit;
    private UUID senderAccountId;
    private UUID receiverAccountId;

    public TransferBean() {
    }

    public TransferBean(UUID id, Integer version, Long amountUnit, UUID senderAccountId, UUID receiverAccountId) {
        this.id = id;
        this.version = version;
        this.amountUnit = amountUnit;
        this.senderAccountId = senderAccountId;
        this.receiverAccountId = receiverAccountId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getAmountUnit() {
        return amountUnit;
    }

    public void setAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
    }

    public UUID getSenderAccountId() {
        return senderAccountId;
    }

    public void setSenderAccountId(UUID senderAccountId) {
        this.senderAccountId = senderAccountId;
    }

    public UUID getReceiverAccountId() {
        return receiverAccountId;
    }

    public void setReceiverAccountId(UUID receiverAccountId) {
        this.receiverAccountId = receiverAccountId;
    }
}

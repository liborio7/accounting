package com.revolut.accounting.persistence.model;

import java.util.UUID;

public class TransferBuilder {
    private UUID id;
    private Integer version;
    private Long amountUnit;
    private UUID senderAccountId;
    private UUID receiverAccountId;

    public TransferBuilder withId(UUID id) {
        this.id = id;
        return this;
    }

    public TransferBuilder withVersion(Integer version) {
        this.version = version;
        return this;
    }

    public TransferBuilder withAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
        return this;
    }

    public TransferBuilder withSenderAccountId(UUID senderAccountId) {
        this.senderAccountId = senderAccountId;
        return this;
    }

    public TransferBuilder withReceiverAccountId(UUID receiverAccountId) {
        this.receiverAccountId = receiverAccountId;
        return this;
    }

    public Transfer build() {
        return new Transfer(id, version, amountUnit, senderAccountId, receiverAccountId);
    }
}
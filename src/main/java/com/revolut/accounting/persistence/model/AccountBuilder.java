package com.revolut.accounting.persistence.model;

import java.util.UUID;

public class AccountBuilder {
    private UUID id;
    private Integer version;
    private Long amountUnit;

    public AccountBuilder withId(UUID id) {
        this.id = id;
        return this;
    }

    public AccountBuilder withVersion(Integer version) {
        this.version = version;
        return this;
    }

    public AccountBuilder withAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
        return this;
    }

    public Account build() {
        return new Account(id, version, amountUnit);
    }
}
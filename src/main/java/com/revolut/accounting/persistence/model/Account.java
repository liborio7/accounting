package com.revolut.accounting.persistence.model;

import java.util.UUID;

public class Account {
    private UUID id;
    private Integer version;
    private Long amountUnit;

    public Account() {
    }

    public Account(UUID id, Integer version, Long amountUnit) {
        this.id = id;
        this.version = version;
        this.amountUnit = amountUnit;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getAmountUnit() {
        return amountUnit;
    }

    public void setAmountUnit(Long amountUnit) {
        this.amountUnit = amountUnit;
    }
}

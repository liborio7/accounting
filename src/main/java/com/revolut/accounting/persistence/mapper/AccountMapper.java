package com.revolut.accounting.persistence.mapper;


import com.revolut.accounting.persistence.model.Account;

import java.util.List;
import java.util.UUID;

public interface AccountMapper {

    Integer insert(Account entity);

    Account loadById(Account entity);

    List<Account> loadLockByIdList(List<UUID> idList);

    List<Account> loadAll();

    Integer updateSkipNull(Account entity);

    void delete(Account entity);
}

package com.revolut.accounting.persistence.mapper;


import com.revolut.accounting.persistence.model.Transfer;

import java.util.List;

public interface TransferMapper {

    Integer insert(Transfer entity);

    Transfer loadById(Transfer entity);

    List<Transfer> loadAll();

}

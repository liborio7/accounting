package com.revolut.accounting.persistence.dao;

import com.revolut.accounting.persistence.mapper.AccountMapper;
import com.revolut.accounting.persistence.model.Account;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Singleton
public class AccountDao {
    private Logger logger = LoggerFactory.getLogger(AccountDao.class);

    @Inject private SqlSessionFactory sqlSessionFactory;

    //============ insert ============//

    public Account insert(Account entity) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            Account result = insert(entity, sqlSession);
            sqlSession.commit();
            return result;
        }
    }

    Account insert(Account entity, SqlSession sqlSession) {
        logger.debug("insert: {}", entity);

        // generate key if not present
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID());
        }

        // insert and check rows affected
        Integer rowsAffected = sqlSession.getMapper(AccountMapper.class).insert(entity);
        if (rowsAffected == 0) {
            throw new PersistenceException();
        }
        return loadById(entity, sqlSession);
    }

    //============ load by id ============//

    public Account loadById(Account entity) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            Account result = loadById(entity, sqlSession);
            sqlSession.commit();
            return result;
        }
    }

    Account loadById(Account entity, SqlSession sqlSession) {
        logger.debug("load by id: {}", entity);
        return sqlSession.getMapper(AccountMapper.class).loadById(entity);
    }

    //============ load lock by id list

    List<Account> loadLockByIdList(List<UUID> idList, SqlSession sqlSession) {
        logger.debug("load by id list: {}", idList);
        if (idList.isEmpty()) return Collections.emptyList();
        return sqlSession.getMapper(AccountMapper.class).loadLockByIdList(idList);
    }

    //============ load all ============//

    public List<Account> loadAll() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Account> result = loadAll(sqlSession);
            sqlSession.commit();
            return result;
        }
    }

    List<Account> loadAll(SqlSession sqlSession) {
        logger.debug("load all");
        return sqlSession.getMapper(AccountMapper.class).loadAll();
    }

    //============ update skip null ============//

    Account updateSkipNull(Account entity, SqlSession sqlSession) {
        logger.debug("update skip null: {}", entity);

        // update and check rows affected
        Integer rowsAffected = sqlSession.getMapper(AccountMapper.class).updateSkipNull(entity);
        if (rowsAffected == 0) {
            throw new PersistenceException();
        }
        return loadById(entity, sqlSession);
    }

    //============ delete ============//

    void delete(Account entity, SqlSession sqlSession) {
        sqlSession.getMapper(AccountMapper.class).delete(entity);
    }
}

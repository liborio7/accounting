package com.revolut.accounting.persistence.dao;

import com.revolut.accounting.business.exception.TransferInsufficientAvailabilityException;
import com.revolut.accounting.business.exception.TransferInvalidActorException;
import com.revolut.accounting.persistence.mapper.TransferMapper;
import com.revolut.accounting.persistence.model.Account;
import com.revolut.accounting.persistence.model.AccountBuilder;
import com.revolut.accounting.persistence.model.Transfer;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Singleton
public class TransferDao {
    private Logger logger = LoggerFactory.getLogger(TransferDao.class);

    @Inject private AccountDao accountDao;
    @Inject private SqlSessionFactory sqlSessionFactory;

    //============ insert ============//

    public Transfer insert(Transfer entity) throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            Transfer result = insert(entity, sqlSession);
            sqlSession.commit();
            return result;
        }
    }

    Transfer insert(Transfer entity, SqlSession sqlSession) throws TransferInvalidActorException, TransferInsufficientAvailabilityException {
        logger.debug("insert: {}", entity);

        // lock and validate actors
        List<Account> actors = accountDao.loadLockByIdList(Arrays.asList(entity.getSenderAccountId(), entity.getReceiverAccountId()), sqlSession);
        Account senderAccount = actors.stream()
                .filter(account -> account.getId().equals(entity.getSenderAccountId()))
                .findFirst()
                .orElseThrow(TransferInvalidActorException::new);
        Account receiverAccount = actors.stream()
                .filter(account -> account.getId().equals(entity.getReceiverAccountId()))
                .findFirst()
                .orElseThrow(TransferInvalidActorException::new);
        if (senderAccount.getId().equals(receiverAccount.getId())) {
            throw new TransferInvalidActorException();
        }

        // validate amount
        if (senderAccount.getAmountUnit() < entity.getAmountUnit()) {
            throw new TransferInsufficientAvailabilityException();
        }

        // update accounts
        accountDao.updateSkipNull(new AccountBuilder()
                .withId(senderAccount.getId())
                .withVersion(senderAccount.getVersion())
                .withAmountUnit(senderAccount.getAmountUnit() - entity.getAmountUnit())
                .build(), sqlSession);
        accountDao.updateSkipNull(new AccountBuilder()
                .withId(receiverAccount.getId())
                .withVersion(receiverAccount.getVersion())
                .withAmountUnit(receiverAccount.getAmountUnit() + entity.getAmountUnit())
                .build(), sqlSession);

        // generate key if not present
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID());
        }

        // insert and check rows affected
        Integer rowsAffected = sqlSession.getMapper(TransferMapper.class).insert(entity);
        if (rowsAffected == 0) {
            throw new PersistenceException();
        }
        return loadById(entity, sqlSession);
    }

    //============ load by id ============//

    public Transfer loadById(Transfer entity) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            Transfer result = loadById(entity, sqlSession);
            sqlSession.commit();
            return result;
        }
    }

    Transfer loadById(Transfer entity, SqlSession sqlSession) {
        logger.debug("load by id: {}", entity);
        return sqlSession.getMapper(TransferMapper.class).loadById(entity);
    }

    //============ load all ============//

    public List<Transfer> loadAll() {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Transfer> result = loadAll(sqlSession);
            sqlSession.commit();
            return result;
        }
    }

    List<Transfer> loadAll(SqlSession sqlSession) {
        logger.debug("load all");
        return sqlSession.getMapper(TransferMapper.class).loadAll();
    }
}

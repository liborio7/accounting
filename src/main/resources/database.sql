-- DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account` (
  `id`          VARCHAR(48),
  `version`     INTEGER,
  `amount_unit` BIGINT,
  PRIMARY KEY (`id`)
);

-- DROP TABLE IF EXISTS `transfer`;
CREATE TABLE IF NOT EXISTS `transfer` (
  `id`                  VARCHAR(48),
  `version`             INTEGER,
  `amount_unit`         BIGINT,
  `sender_account_id`   VARCHAR(48),
  `receiver_account_id` VARCHAR(48),
  PRIMARY KEY (`id`)
);
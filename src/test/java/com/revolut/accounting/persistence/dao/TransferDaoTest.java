package com.revolut.accounting.persistence.dao;

import com.revolut.accounting.business.exception.TransferInsufficientAvailabilityException;
import com.revolut.accounting.business.exception.TransferInvalidActorException;
import com.revolut.accounting.persistence.model.Account;
import com.revolut.accounting.persistence.model.AccountBuilder;
import com.revolut.accounting.persistence.model.Transfer;
import com.revolut.accounting.persistence.model.TransferBuilder;
import org.apache.commons.lang3.RandomUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferDaoTest extends BaseDaoTest {

    @Inject private AccountDao accountDao;
    @Inject private TransferDao dao;

    //============ insert and load by id

    @Test
    public void shouldInsertAndLoadById() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // insert actor
        SqlSession session = sqlSessionFactory.openSession();
        Account sender = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);
        Account receiver = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);

        // given
        Transfer toInsert = new TransferBuilder()
                .withSenderAccountId(sender.getId())
                .withReceiverAccountId(receiver.getId())
                .withAmountUnit(RandomUtils.nextLong(1, sender.getAmountUnit()))
                .build();
        Transfer inserted = dao.insert(toInsert, session);
        Transfer toLoad = new TransferBuilder()
                .withId(inserted.getId())
                .build();

        // when
        Account updatedSender = accountDao.loadById(sender, session);
        Account updatedReceiver = accountDao.loadById(receiver, session);
        Transfer loaded = dao.loadById(toLoad, session);

        // then
        assertThat(loaded).isEqualToIgnoringGivenFields(inserted);
        assertThat(updatedSender.getAmountUnit()).isEqualTo(sender.getAmountUnit() - inserted.getAmountUnit());
        assertThat(updatedReceiver.getAmountUnit()).isEqualTo(receiver.getAmountUnit() + inserted.getAmountUnit());
    }

    @Test(expected = TransferInvalidActorException.class)
    public void shouldThrowInvalidActorOnInsertDueToWrongSender() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // insert actor
        SqlSession session = sqlSessionFactory.openSession();
        Account receiver = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);

        // given
        Transfer toInsert = new TransferBuilder()
                .withSenderAccountId(UUID.randomUUID())
                .withReceiverAccountId(receiver.getId())
                .withAmountUnit(RandomUtils.nextLong(1, 100))
                .build();
        dao.insert(toInsert, session);
    }

    @Test(expected = TransferInvalidActorException.class)
    public void shouldThrowInvalidActorOnInsertDueToWrongReceiver() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // insert actor
        SqlSession session = sqlSessionFactory.openSession();
        Account sender = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);

        // given
        Transfer toInsert = new TransferBuilder()
                .withSenderAccountId(sender.getId())
                .withReceiverAccountId(UUID.randomUUID())
                .withAmountUnit(RandomUtils.nextLong(1, 100))
                .build();
        dao.insert(toInsert, session);
    }

    @Test(expected = TransferInvalidActorException.class)
    public void shouldThrowInvalidActorOnInsertDueToSameActor() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // insert actor
        SqlSession session = sqlSessionFactory.openSession();
        Account actor = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);

        // given
        Transfer toInsert = new TransferBuilder()
                .withSenderAccountId(actor.getId())
                .withReceiverAccountId(actor.getId())
                .withAmountUnit(RandomUtils.nextLong(1, actor.getAmountUnit()))
                .build();
        dao.insert(toInsert, session);
    }

    @Test(expected = TransferInsufficientAvailabilityException.class)
    public void shouldThrowInsufficientAvailabilityOnInsert() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // insert actor
        SqlSession session = sqlSessionFactory.openSession();
        Account sender = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);
        Account receiver = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(100, 1000))
                .build(), session);

        // given
        Transfer toInsert = new TransferBuilder()
                .withSenderAccountId(sender.getId())
                .withReceiverAccountId(receiver.getId())
                .withAmountUnit(RandomUtils.nextLong(sender.getAmountUnit() + 1, sender.getAmountUnit() + 100))
                .build();
        dao.insert(toInsert, session);
    }

    //============ load all

    @Test
    public void shouldLoadAll() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // insert actor
        SqlSession session = sqlSessionFactory.openSession();
        Account sender = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(1000L)
                .build(), session);
        Account receiver = accountDao.insert(random(AccountBuilder.class)
                .withAmountUnit(1000L)
                .build(), session);

        // insert transfers
        List<Transfer> toInsert = IntStream.range(1, 10)
                .mapToObj(__ -> new TransferBuilder()
                        .withSenderAccountId(sender.getId())
                        .withReceiverAccountId(receiver.getId())
                        .withAmountUnit(1L)
                        .build())
                .collect(Collectors.toList());
        List<Transfer> inserted = new ArrayList<>();
        for (Transfer t : toInsert) {
            inserted.add(dao.insert(t, session));
        }

        // load
        List<Transfer> loaded = dao.loadAll(session);

        // check loaded transfers
        assertThat(inserted.stream().allMatch(
                insert -> loaded.stream().anyMatch(
                        load -> load.getId().equals(insert.getId()) &&
                                load.getVersion().equals(insert.getVersion()) &&
                                load.getSenderAccountId().equals(insert.getSenderAccountId()) &&
                                load.getReceiverAccountId().equals(insert.getReceiverAccountId()) &&
                                load.getAmountUnit().equals(insert.getAmountUnit()))
        )).isTrue();
    }
}
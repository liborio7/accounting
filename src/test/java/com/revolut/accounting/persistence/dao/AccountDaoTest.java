package com.revolut.accounting.persistence.dao;

import com.revolut.accounting.persistence.model.Account;
import com.revolut.accounting.persistence.model.AccountBuilder;
import org.apache.commons.lang3.RandomUtils;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static io.github.benas.randombeans.api.EnhancedRandom.randomListOf;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountDaoTest extends BaseDaoTest {

    @Inject private AccountDao dao;

    //============ insert and load by id

    @Test
    public void shouldInsertAndLoadById() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        Account toInsert = random(Account.class);
        Account inserted = dao.insert(toInsert, session);
        Account toLoad = new AccountBuilder()
                .withId(inserted.getId())
                .build();

        // when
        Account loaded = dao.loadById(toLoad, session);

        // then
        assertThat(loaded).isEqualToIgnoringGivenFields(inserted);
    }

    //============ load lock by id list

    @Test
    public void shouldLoadLockByIdList() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        List<Account> toInsert = randomListOf(5, Account.class);
        List<Account> inserted = toInsert.stream()
                .map(a -> dao.insert(a, session))
                .collect(Collectors.toList());
        List<UUID> toLoad = inserted.stream()
                .map(Account::getId)
                .collect(Collectors.toList());

        // when
        List<Account> loaded = dao.loadLockByIdList(toLoad, session);

        // then
        assertThat(loaded).hasSameSizeAs(inserted);
        assertThat(inserted.stream().allMatch(
                insert -> loaded.stream().anyMatch(
                        load -> load.getId().equals(insert.getId()) &&
                                load.getVersion().equals(insert.getVersion()) &&
                                load.getAmountUnit().equals(insert.getAmountUnit()))
        )).isTrue();
    }

    //============ load all

    @Test
    public void shouldLoadAll() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        List<Account> toInsert = randomListOf(RandomUtils.nextInt(5, 10), Account.class);
        List<Account> inserted = toInsert.stream().map(a -> dao.insert(a, session)).collect(Collectors.toList());

        // when
        List<Account> loaded = dao.loadAll(session);

        // then
        assertThat(inserted.stream().allMatch(
                insert -> loaded.stream().anyMatch(
                        load -> load.getId().equals(insert.getId()) &&
                                load.getVersion().equals(insert.getVersion()) &&
                                load.getAmountUnit().equals(insert.getAmountUnit()))
        )).isTrue();
    }

    //============ update skip null

    @Test
    public void shouldUpdateSkipNull() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        Account toInsert = random(Account.class);
        Account inserted = dao.insert(toInsert, session);
        Account toUpdate = new AccountBuilder()
                .withId(inserted.getId())
                .withVersion(inserted.getVersion())
                .withAmountUnit(RandomUtils.nextLong(100, 200))
                .build();

        // when
        dao.updateSkipNull(toUpdate, session);

        // then
        Account loadAfterUpdate = dao.loadById(inserted, session);
        assertThat(loadAfterUpdate.getId()).isEqualTo(toUpdate.getId());
        assertThat(loadAfterUpdate.getVersion()).isEqualTo(toUpdate.getVersion() + 1);
        assertThat(loadAfterUpdate.getAmountUnit()).isEqualTo(toUpdate.getAmountUnit());
    }

    @Test(expected = PersistenceException.class)
    public void shouldThrowPersistenceExceptionOnUpdateSkipNullDueToNullId() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        Account toInsert = random(Account.class);
        Account inserted = dao.insert(toInsert, session);
        Account toUpdate = random(AccountBuilder.class)
                .withVersion(inserted.getVersion())
                .build();

        // when
        dao.updateSkipNull(toUpdate, session);
    }

    @Test(expected = PersistenceException.class)
    public void shouldThrowPersistenceExceptionOnUpdateSkipNullDueToNullVersion() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        Account toInsert = random(Account.class);
        Account inserted = dao.insert(toInsert, session);
        Account toUpdate = random(AccountBuilder.class)
                .withId(inserted.getId())
                .build();

        // when
        dao.updateSkipNull(toUpdate, session);
    }

    @Test(expected = PersistenceException.class)
    public void shouldThrowExceptionOnUpdateSkipNullDueToLock() {
        // insert and commit
        Account toInsert = random(Account.class);
        Account inserted;
        try (SqlSession insertSession = sqlSessionFactory.openSession()) {
            inserted = dao.insert(toInsert, insertSession);
            insertSession.commit();
        }

        // prepare load and update
        Account toLoad = new AccountBuilder()
                .withId(inserted.getId())
                .build();
        Account toUpdate = new AccountBuilder()
                .withId(inserted.getId())
                .withVersion(inserted.getVersion())
                .withAmountUnit(RandomUtils.nextLong(100, 200))
                .build();

        // open 2 sessions
        try (SqlSession lockSession = sqlSessionFactory.openSession();
             SqlSession updateSession = sqlSessionFactory.openSession()) {

            // load lock
            dao.loadLockByIdList(Collections.singletonList(toLoad.getId()), lockSession);

            // try to update the same locked record
            dao.updateSkipNull(toUpdate, updateSession);

        } finally {
            // delete to clear database
            SqlSession deleteSession = sqlSessionFactory.openSession();
            Account toDelete = new AccountBuilder()
                    .withId(inserted.getId())
                    .build();
            dao.delete(toDelete, deleteSession);
            deleteSession.commit();
        }
    }

    //============ delete

    @Test
    public void shouldDelete() {
        // given
        SqlSession session = sqlSessionFactory.openSession();
        Account toInsert = random(Account.class);
        Account inserted = dao.insert(toInsert, session);
        Account toDelete = new AccountBuilder()
                .withId(inserted.getId())
                .build();

        // when
        dao.delete(toDelete, session);

        Account toLoad = new AccountBuilder()
                .withId(inserted.getId())
                .build();
        Account loaded = dao.loadById(toLoad, session);

        // then
        assertThat(loaded).isNull();
    }
}
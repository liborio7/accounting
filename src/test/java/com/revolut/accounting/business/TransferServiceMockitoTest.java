package com.revolut.accounting.business;

import com.revolut.accounting.business.bean.TransferBean;
import com.revolut.accounting.business.bean.TransferBeanBuilder;
import com.revolut.accounting.business.converter.TransferConverter;
import com.revolut.accounting.business.exception.TransferInsufficientAvailabilityException;
import com.revolut.accounting.business.exception.TransferInvalidActorException;
import com.revolut.accounting.persistence.dao.TransferDao;
import com.revolut.accounting.persistence.model.Transfer;
import com.revolut.accounting.persistence.model.TransferBuilder;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.UUID;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static io.github.benas.randombeans.api.EnhancedRandom.randomListOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceMockitoTest {

    @Mock private TransferDao dao;
    @Mock private TransferConverter converter;
    @InjectMocks private TransferService service;

    @Before
    public void setUp() {
        when(converter.modelToBean(any())).thenCallRealMethod();
        when(converter.beanToModel(any())).thenCallRealMethod();
    }

    //============ insert

    @Test
    public void shouldInsert() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // given
        TransferBean transferBean = new TransferBeanBuilder()
                .withAmountUnit(RandomUtils.nextLong(1, 1000))
                .withSenderAccountId(UUID.randomUUID())
                .withReceiverAccountId(UUID.randomUUID())
                .build();
        Transfer transfer = new TransferBuilder()
                .withId(UUID.randomUUID())
                .withVersion(0)
                .withSenderAccountId(transferBean.getSenderAccountId())
                .withReceiverAccountId(transferBean.getReceiverAccountId())
                .withAmountUnit(transferBean.getAmountUnit())

                .build();

        // when
        when(dao.insert(any())).thenReturn(transfer);

        TransferBean result = service.insert(transferBean);

        // then
        assertThat(result).isNotNull();
        assertThat(result).isEqualToIgnoringNullFields(transferBean);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToNullBean() {
        // when
        service.insert(null);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToNullAmount() {
        // given
        TransferBean transferBean = random(TransferBeanBuilder.class)
                .withAmountUnit(null)
                .build();

        // when
        service.insert(transferBean);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToInvalidAmount() {
        // given
        TransferBean transferBean = random(TransferBeanBuilder.class)
                .withAmountUnit(-RandomUtils.nextLong(1, 1000))
                .build();

        // when
        service.insert(transferBean);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToNullSender() {
        // given
        TransferBean transferBean = random(TransferBeanBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(1, 1000))
                .withSenderAccountId(null)
                .build();

        // when
        service.insert(transferBean);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToNullReceiver() {
        // given
        TransferBean transferBean = random(TransferBeanBuilder.class)
                .withAmountUnit(RandomUtils.nextLong(1, 1000))
                .withReceiverAccountId(null)
                .build();

        // when
        service.insert(transferBean);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToInsufficientAvailability() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // given
        TransferBean transferBean = new TransferBeanBuilder()
                .withAmountUnit(RandomUtils.nextLong(1, 1000))
                .withSenderAccountId(UUID.randomUUID())
                .withReceiverAccountId(UUID.randomUUID())
                .build();

        // when
        when(dao.insert(any())).thenThrow(TransferInsufficientAvailabilityException.class);

        service.insert(transferBean);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToInvalidActor() throws TransferInsufficientAvailabilityException, TransferInvalidActorException {
        // given
        TransferBean transferBean = new TransferBeanBuilder()
                .withAmountUnit(RandomUtils.nextLong(1, 1000))
                .withSenderAccountId(UUID.randomUUID())
                .withReceiverAccountId(UUID.randomUUID())
                .build();

        // when
        when(dao.insert(any())).thenThrow(TransferInvalidActorException.class);

        service.insert(transferBean);
    }

    //============ load all

    @Test
    public void shouldLoadAll() {
        // given
        List<Transfer> transferList = randomListOf(RandomUtils.nextInt(5, 10), Transfer.class);

        // when
        when(dao.loadAll()).thenReturn(transferList);

        List<TransferBean> result = service.loadAll();

        // then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).hasSameSizeAs(transferList);
    }

    //============ load by id

    @Test
    public void shouldLoadById() {
        // given
        UUID id = UUID.randomUUID();
        Transfer transfer = random(TransferBuilder.class)
                .withId(id)
                .build();

        // when
        when(dao.loadById(argThat(a -> a.getId().equals(id)))).thenReturn(transfer);

        TransferBean result = service.loadById(id);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(id);
    }
}

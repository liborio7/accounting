package com.revolut.accounting.business;

import com.revolut.accounting.business.bean.AccountBean;
import com.revolut.accounting.business.bean.AccountBeanBuilder;
import com.revolut.accounting.business.converter.AccountConverter;
import com.revolut.accounting.persistence.dao.AccountDao;
import com.revolut.accounting.persistence.model.Account;
import com.revolut.accounting.persistence.model.AccountBuilder;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.UUID;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static io.github.benas.randombeans.api.EnhancedRandom.randomListOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceMockitoTest {

    @Mock private AccountDao dao;
    @Mock private AccountConverter converter;
    @InjectMocks private AccountService service;

    @Before
    public void setUp() {
        when(converter.modelToBean(any())).thenCallRealMethod();
        when(converter.beanToModel(any())).thenCallRealMethod();
    }

    //============ insert

    @Test
    public void shouldInsert() {
        // given
        AccountBean accountBean = new AccountBeanBuilder()
                .withAmountUnit(RandomUtils.nextLong(1, 1000))
                .build();
        Account account = new AccountBuilder()
                .withId(UUID.randomUUID())
                .withVersion(0)
                .withAmountUnit(accountBean.getAmountUnit())
                .build();

        // when
        when(dao.insert(any())).thenReturn(account);

        AccountBean result = service.insert(accountBean);

        // then
        assertThat(result).isNotNull();
        assertThat(result).isEqualToIgnoringNullFields(accountBean);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToNullBean() {
        // when
        service.insert(null);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToNullAmount() {
        // given
        AccountBean accountBean = new AccountBeanBuilder()
                .build();

        // when
        service.insert(accountBean);

        // then
        verifyZeroInteractions(dao);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestOnInsertDueToInvalidAmount() {
        // given
        AccountBean accountBean = new AccountBeanBuilder()
                .withAmountUnit(-RandomUtils.nextLong(1, 1000))
                .build();

        // when
        service.insert(accountBean);

        // then
        verifyZeroInteractions(dao);
    }

    //============ load all

    @Test
    public void shouldLoadAll() {
        // given
        List<Account> accountList = randomListOf(RandomUtils.nextInt(5, 10), Account.class);

        // when
        when(dao.loadAll()).thenReturn(accountList);

        List<AccountBean> result = service.loadAll();

        // then
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).hasSameSizeAs(accountList);
    }

    //============ load by id

    @Test
    public void shouldLoadById() {
        // given
        UUID id = UUID.randomUUID();
        Account account = random(AccountBuilder.class)
                .withId(id)
                .build();

        // when
        when(dao.loadById(argThat(a -> a.getId().equals(id)))).thenReturn(account);

        AccountBean result = service.loadById(id);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(id);
    }
}

package com.revolut.accounting.business.converter;

import com.revolut.accounting.business.bean.AccountBean;
import com.revolut.accounting.persistence.model.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AccountConverterMockitoTest {

    @InjectMocks private AccountConverter converter;

    @Test
    public void shouldConvertModelToBean() {
        // given
        Account model = random(Account.class);

        // when
        AccountBean bean = converter.modelToBean(model);

        // then
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isEqualTo(model.getId());
        assertThat(bean.getVersion()).isEqualTo(model.getVersion());
        assertThat(bean.getAmountUnit()).isEqualTo(model.getAmountUnit());
    }

    @Test
    public void shouldConvertBeanToModel() {
        // given
        AccountBean bean = random(AccountBean.class);

        // when
        Account model = converter.beanToModel(bean);

        // then
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(bean.getId());
        assertThat(model.getVersion()).isEqualTo(bean.getVersion());
        assertThat(model.getAmountUnit()).isEqualTo(bean.getAmountUnit());
    }

}
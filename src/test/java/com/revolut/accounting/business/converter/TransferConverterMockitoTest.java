package com.revolut.accounting.business.converter;

import com.revolut.accounting.business.bean.TransferBean;
import com.revolut.accounting.persistence.model.Transfer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static io.github.benas.randombeans.api.EnhancedRandom.random;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TransferConverterMockitoTest {

    @InjectMocks private TransferConverter converter;

    @Test
    public void shouldConvertModelToBean() {
        // given
        Transfer model = random(Transfer.class);

        // when
        TransferBean bean = converter.modelToBean(model);

        // then
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isEqualTo(model.getId());
        assertThat(bean.getVersion()).isEqualTo(model.getVersion());
        assertThat(bean.getAmountUnit()).isEqualTo(model.getAmountUnit());
        assertThat(bean.getSenderAccountId()).isEqualTo(model.getSenderAccountId());
        assertThat(bean.getReceiverAccountId()).isEqualTo(model.getReceiverAccountId());
    }

    @Test
    public void shouldConvertBeanToModel() {
        // given
        TransferBean bean = random(TransferBean.class);

        // when
        Transfer model = converter.beanToModel(bean);

        // then
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(bean.getId());
        assertThat(model.getVersion()).isEqualTo(bean.getVersion());
        assertThat(model.getAmountUnit()).isEqualTo(bean.getAmountUnit());
        assertThat(model.getSenderAccountId()).isEqualTo(bean.getSenderAccountId());
        assertThat(model.getReceiverAccountId()).isEqualTo(bean.getReceiverAccountId());
    }
}
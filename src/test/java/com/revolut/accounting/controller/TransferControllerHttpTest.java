package com.revolut.accounting.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.revolut.accounting.business.bean.AccountBean;
import com.revolut.accounting.business.bean.AccountBeanBuilder;
import com.revolut.accounting.business.bean.TransferBean;
import com.revolut.accounting.business.bean.TransferBeanBuilder;
import com.revolut.accounting.config.bean.ErrorResponseBean;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferControllerHttpTest extends BaseControllerTest {

    private static final String ACCOUNTS_URL = "v1/accounts";
    private static final String TRANSFERS_URL = "v1/transfers";

    private AccountBean insertAccount(Long amountUnit) throws JsonProcessingException {
        AccountBean senderAccountToInsert = new AccountBeanBuilder()
                .withAmountUnit(amountUnit)
                .build();
        Response responseSenderAccountInsert = target(ACCOUNTS_URL).request().post(Entity.json(objectMapper.writeValueAsString(senderAccountToInsert)));
        return objectMapper.convertValue(responseSenderAccountInsert.readEntity(Object.class), AccountBean.class);
    }

    @Test
    public void shouldInsertAndLoad() throws JsonProcessingException {
        // given
        AccountBean senderAccount = insertAccount(RandomUtils.nextLong(100, 200));
        AccountBean receiverAccount = insertAccount(RandomUtils.nextLong(100, 200));

        // test insert
        TransferBean transferToInsert = new TransferBeanBuilder()
                .withSenderAccountId(senderAccount.getId())
                .withReceiverAccountId(receiverAccount.getId())
                .withAmountUnit(senderAccount.getAmountUnit())
                .build();
        Response responsePost = target(TRANSFERS_URL).request().post(Entity.json(objectMapper.writeValueAsString(transferToInsert)));
        assertThat(responsePost.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        // convert and assert insert response
        TransferBean inserted = objectMapper.convertValue(responsePost.readEntity(Object.class), TransferBean.class);
        assertThat(inserted).isNotNull();
        assertThat(inserted.getId()).isNotNull();
        assertThat(inserted.getAmountUnit()).isEqualTo(transferToInsert.getAmountUnit());
        assertThat(inserted.getVersion()).isNotNull();

        // test load by id
        UUID insertedId = inserted.getId();
        Response responseGetById = target(TRANSFERS_URL).path(insertedId.toString()).request().get();
        assertThat(responseGetById.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        // convert and assert load by id
        TransferBean loaded = objectMapper.convertValue(responseGetById.readEntity(Object.class), TransferBean.class);
        assertThat(loaded).isNotNull();
        assertThat(loaded).isEqualToComparingFieldByField(inserted);

        // test load all
        Response responseGetAll = target(TRANSFERS_URL).request().get();
        assertThat(responseGetAll.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        // convert and assert load all
        ArrayList<TransferBean> all = objectMapper.convertValue(
                responseGetAll.readEntity(ArrayList.class),
                new TypeReference<ArrayList<TransferBean>>() {
                });

        assertThat(all).isNotNull();
        assertThat(all).isNotEmpty();
        assertThat(all.stream().anyMatch(transfer ->
                transfer.getId().equals(inserted.getId()) &&
                        transfer.getAmountUnit().equals(inserted.getAmountUnit()) &&
                        transfer.getVersion().equals(inserted.getVersion())
        ));
    }

    @Test
    public void shouldThrowBadRequestDueToInvalidActor() throws JsonProcessingException {
        // given
        AccountBean senderAccount = insertAccount(RandomUtils.nextLong(100, 200));
        TransferBean transferToInsert = new TransferBeanBuilder()
                .withSenderAccountId(senderAccount.getId())
                .withReceiverAccountId(UUID.randomUUID())
                .withAmountUnit(senderAccount.getAmountUnit())
                .build();

        // when
        Response responsePost = target(TRANSFERS_URL).request().post(Entity.json(objectMapper.writeValueAsString(transferToInsert)));

        // then
        assertThat(responsePost.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorResponseBean errorResponseBean = objectMapper.convertValue(responsePost.readEntity(Object.class), ErrorResponseBean.class);
        assertThat(errorResponseBean.getCode()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        assertThat(errorResponseBean.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getReasonPhrase());
    }

    @Test
    public void shouldThrowBadRequestDueToInsufficientAvailability() throws JsonProcessingException {
        // given
        AccountBean senderAccount = insertAccount(RandomUtils.nextLong(0, 10));
        AccountBean receiverAccount = insertAccount(RandomUtils.nextLong(100, 200));
        TransferBean transferToInsert = new TransferBeanBuilder()
                .withSenderAccountId(senderAccount.getId())
                .withReceiverAccountId(receiverAccount.getId())
                .withAmountUnit(senderAccount.getAmountUnit() + RandomUtils.nextLong(100, 200))
                .build();

        // when
        Response responsePost = target(TRANSFERS_URL).request().post(Entity.json(objectMapper.writeValueAsString(transferToInsert)));

        // then
        assertThat(responsePost.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorResponseBean errorResponseBean = objectMapper.convertValue(responsePost.readEntity(Object.class), ErrorResponseBean.class);
        assertThat(errorResponseBean.getCode()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        assertThat(errorResponseBean.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getReasonPhrase());
    }

}
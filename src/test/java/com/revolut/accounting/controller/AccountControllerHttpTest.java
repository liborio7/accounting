package com.revolut.accounting.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.revolut.accounting.business.bean.AccountBean;
import com.revolut.accounting.business.bean.AccountBeanBuilder;
import com.revolut.accounting.config.bean.ErrorResponseBean;
import org.junit.Test;
import org.jvnet.hk2.annotations.Service;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Service
public class AccountControllerHttpTest extends BaseControllerTest {

    private static final String ACCOUNTS_URL = "v1/accounts";

    @Test
    public void shouldInsertAndLoad() throws JsonProcessingException {
        // test insert
        AccountBean accountToInsert = new AccountBeanBuilder()
                .withAmountUnit(100L)
                .build();
        Response responsePost = target(ACCOUNTS_URL).request().post(Entity.json(objectMapper.writeValueAsString(accountToInsert)));
        assertThat(responsePost.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        // convert and assert insert response
        AccountBean inserted = objectMapper.convertValue(responsePost.readEntity(Object.class), AccountBean.class);
        assertThat(inserted).isNotNull();
        assertThat(inserted.getId()).isNotNull();
        assertThat(inserted.getAmountUnit()).isEqualTo(accountToInsert.getAmountUnit());
        assertThat(inserted.getVersion()).isNotNull();

        // test load by id
        UUID insertedId = inserted.getId();
        Response responseGetById = target(ACCOUNTS_URL).path(insertedId.toString()).request().get();
        assertThat(responseGetById.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        // convert and assert load by id
        AccountBean loaded = objectMapper.convertValue(responseGetById.readEntity(Object.class), AccountBean.class);
        assertThat(loaded).isNotNull();
        assertThat(loaded).isEqualToComparingFieldByField(inserted);

        // test load all
        Response responseGetAll = target(ACCOUNTS_URL).request().get();
        assertThat(responseGetAll.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        // convert and assert load all
        ArrayList<AccountBean> all = objectMapper.convertValue(
                responseGetAll.readEntity(ArrayList.class),
                new TypeReference<ArrayList<AccountBean>>() {
                });

        assertThat(all).isNotNull();
        assertThat(all).isNotEmpty();
        assertThat(all.stream().anyMatch(account ->
                account.getId().equals(inserted.getId()) &&
                        account.getAmountUnit().equals(inserted.getAmountUnit()) &&
                        account.getVersion().equals(inserted.getVersion())
        ));
    }

    @Test
    public void shouldThrowBadRequestOnInvalidAmount() throws JsonProcessingException {
        // test insert
        AccountBean accountToInsert = new AccountBeanBuilder()
                .withAmountUnit(-100L)
                .build();

        // when
        Response responsePost = target(ACCOUNTS_URL).request().post(Entity.json(objectMapper.writeValueAsString(accountToInsert)));

        // then
        assertThat(responsePost.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorResponseBean errorResponseBean = objectMapper.convertValue(responsePost.readEntity(Object.class), ErrorResponseBean.class);
        assertThat(errorResponseBean.getCode()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        assertThat(errorResponseBean.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getReasonPhrase());
    }
}